﻿/*Markov Chain Amusement
 * 
 * Written by Travis Manning
 * All on 12/16/2014
 *
 * Uses a Markov Chain generalization to validate word spelling. Included wordlist only spans ~1.82 MB, but the premise
 * should be somewhat solid. Count frequency of letter transitions, and use them to validate a spelling at run-time (probabilistic.)
 * 
 * W-O-R-D => W->O Trans
 *            O->R Trans
 *            R->D Trans
 * 
 * Then generate some frequencies.
 * 
 * 10-18-14: Added input sanitization.
 */

using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public static class Extensions
{
    /**
     * <summary>Takes a 2-D Array and computes the sum of its elements.</summary>
     * <param name="x">int[,] Array to be looped over and summed.</param>
     * <returns>Returns an int equal to the sum of all elements.</returns>
     * <example>sum({{1,2},{3,4}})==10</example>
    */
    static public int Sum(this int[,] x) //summation of 2D array
    {
        int r = 0;
        for (int i = 0; i < 26; i++) { for (int j = 0; j < 26; j++) { r += x[i, j]; } }
        return r;
    }

    static public bool Sanitize(this char[] a,int chrmin = 65, int chrmax = 97+26) //only allows 
    {
        for (int i = 0; i<a.Length; i++){if ((int)a[i]<chrmin || (int)a[i]>chrmax){return false;}}
        return true;
    }
}

namespace ConsoleApplication3
{
    class Program
    {
        public const int LOWER_A_ASCII = 97;
        public const int TRANSIT_TO_BLOCK = 100;

        public static int[,] GenerateFrequencyMatrix(string wlp)
        {
            //declare and initialize vars
            string WordListComplete = "";
            string[] WordList;
            char[] chrary;
            int[,] fm = new int[26, 26];
            for (int i = 0; i < 26; i++) { for (int j = 0; j < 26; j++) { fm[i, j] = 0; } }

            //read in file
            try { using (StreamReader sr = new StreamReader(wlp)) { WordListComplete = sr.ReadToEnd(); } }
            catch (Exception e)
            {
                Console.WriteLine("The file could not be read:\n::{0}::", e.Message);
                Console.ReadLine();
                System.Environment.Exit(0);
            }

            //process file into word list, then word list into transition frequencies
            WordList = WordListComplete.Split('\n', '\r'); //split up list
            foreach (string word in WordList)
            {
                chrary = word.ToCharArray(); //split up words
                for (int j = 1; j < word.Length; j++) { fm[(int)chrary[j - 1] - LOWER_A_ASCII, 
                    (int)chrary[j] - LOWER_A_ASCII]++; }
            }  
 
            return fm;
        }

        static int Main(string[] args)
        {
            
            //Vars (declare and initialize)
            string WordListPath = @"C:\Users\tmanning\Downloads\enable1.txt";
            char[] InputString;
            int total;
            int[,] FrequencyMatrix = new int[26, 26];
            float[] Probabilities, MaxBadFrequency;
            float[,] AcceptabilityMatrix = new float[26,26];


            //Generate frequency table
            FrequencyMatrix = GenerateFrequencyMatrix(WordListPath);

            
            //Convert frequencies to acceptability percentages
            total = FrequencyMatrix.Sum();
            for (int i = 0; i < 26; i++) { for (int j = 0; j < 26; j++) { AcceptabilityMatrix[i, j] = 
                (float)FrequencyMatrix[i, j] / total * 100; } }


            //Get word to check
            input:
            Console.Write("Word to check: ");
            InputString = Console.ReadLine().ToLower().ToCharArray();
            if (Array.Equals("".ToCharArray(),InputString) || InputString.Sanitize()){return 1;} //Exit program if user enters a blank string
            Probabilities = new float[InputString.Length - 1];


            //populate probabilities array with transition probabilities from acceptability matrix
            for (int j = 1; j < InputString.Length; j++) { Probabilities[j-1] = 
                AcceptabilityMatrix[(int)InputString[j - 1] - LOWER_A_ASCII, 
                (int)InputString[j] - LOWER_A_ASCII]; }
             

            //For no real reason, I've elected to select the 20 lowest percentages and call that the line.
            //EDIT: 20 isn't good enough. Random character strings get false positive. changed to 100.
            total = 0;
            MaxBadFrequency = new float[676];
            for (int j = 0; j < 676; j++) 
            {
                MaxBadFrequency[j] = AcceptabilityMatrix[j / 26, j % 26];
            }
            Array.Sort(MaxBadFrequency); //sorts so that smallest element is at index 0

            for (int j = 0; j < Probabilities.Length; j++ )
            {
                if (Probabilities[j] <= MaxBadFrequency[TRANSIT_TO_BLOCK])
                {
                    Console.WriteLine("Word is probably misspelled.");
                    Console.WriteLine("Transition from {0} to {1} only has a probability of {2}.", 
                        InputString[j], InputString[j + 1], Math.Round(Probabilities[j],7));
                    goto input;
                }
            }
            Console.WriteLine("Word is probably spelled correctly.");
            goto input;
        }
    }
}
